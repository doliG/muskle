import { Router, Request, Response, NextFunction } from 'express';
import Post from '../models/Post';

class PostRouter {
	router: Router;

	constructor() {
		this.router = Router();
		this.routes();
	}

	routes(): void {
		this.router.get('/', this.getPosts);
		this.router.get('/:slug', this.getPost);
	}

	getPosts(req: Request, res: Response): void {
		Post.find({})
			.then((data) => {
				const { statusMessage, statusCode } = req;
				res.status(200).json({ data, statusMessage, statusCode });
			})
			.catch((err) => {
				const { statusMessage, statusCode } = req;
				res.status(statusCode).json({ statusMessage });
			});
	}

	getPost(req: Request, res: Response): void {}
	createPost(req: Request, res: Response): void {}
	updatePost(req: Request, res: Response): void {}
	deletePosts(req: Request, res: Response): void {}
}

const postRoutes = new PostRouter();
postRoutes.routes();

export default postRoutes.router;